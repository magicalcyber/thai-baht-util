package cc.magickiat.util.thaibaht;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.apache.commons.lang3.ArrayUtils;

public class ThaiBahtUtil {

	private static final String BAHT_TEXT = "บาท";
	private static final String SATANG_TEXT = "สตางค์";

	private static final String[] THAI_NUMBER = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด",
			"เก้า" };
	private static final String[] UNIT_SINGLE = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };

	public static String convertToText(BigDecimal amount) {
		System.out.println("Convert amount: " + amount);
		if (amount.longValue() > Integer.MAX_VALUE) {
			return String.format("ไม่สามารถรองรับค่าที่เกินขนาด %s ได้",
					new DecimalFormat("#,###").format(Integer.MAX_VALUE));
		}

		StringBuilder sb = new StringBuilder();

		BigDecimal baseAmount = amount;
		BigDecimal floatAmount = null;
		if (amount.scale() > 0) {
			amount = amount.setScale(2, RoundingMode.HALF_UP);
			BigDecimal[] compound = amount.divideAndRemainder(BigDecimal.ONE);
			baseAmount = compound[0].setScale(0);
			floatAmount = compound[1].movePointRight(2);
			if (floatAmount.equals(BigDecimal.ZERO)) {
				floatAmount = null;
			}
		}

		convertBaseAmountToText(sb, baseAmount);

		if (floatAmount != null) {
			convertFloatAmountToText(sb, floatAmount);
		}

		return sb.toString();
	}

	private static void convertNumberToText(StringBuilder sb, BigDecimal baseAmount) {
		if (baseAmount == null) {
			return;
		}

		char[] charArray = baseAmount.toString().toCharArray();
		ArrayUtils.reverse(charArray);

		try {
			for (int index = charArray.length - 1; index >= 0; index--) {

				int position = index > 6 ? index % 6 : index;

				int num = convertCharToInt(charArray[index]);

				if (charArray.length > 1 && num == 0 && position < 6) {
					continue;
				}

				if (charArray.length > 1 && num == 0 && position % 6 == 0) {
					sb.append(getUnitText(position));
					continue;
				}

				if (charArray.length > 1 && position == 0 && num == 1) {
					sb.append("เอ็ด");
					continue;
				}

				if (charArray.length > 1 && num == 1 && position == 1) {
					sb.append(getUnitText(position));
					continue;
				}

				if (charArray.length > 1 && position == 1 && num == 2) {
					sb.append("ยี่");
					sb.append(getUnitText(position));
					continue;
				}

				sb.append(getDigitText(num));
				sb.append(getUnitText(position));

			}
		} catch (Exception e) {
			System.err.println(baseAmount);
			System.err.println(e);
		}

	}

	private static void convertBaseAmountToText(StringBuilder sb, BigDecimal baseAmount) {
		convertNumberToText(sb, baseAmount);
		sb.append(BAHT_TEXT);
	}

	private static void convertFloatAmountToText(StringBuilder sb, BigDecimal baseAmount) {
		convertNumberToText(sb, baseAmount);
		sb.append(SATANG_TEXT);
	}

	private static int convertCharToInt(char c) {
		return Integer.parseInt(Character.toString(c));
	}

	private static String getDigitText(int num) {
		return THAI_NUMBER[num];
	}

	private static String getUnitText(int index) {
		if (index <= 6) {
			return UNIT_SINGLE[index];
		}

		return UNIT_SINGLE[index % 6];
	}

}
