package cc.magickiat.util.thaibaht;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.junit.Test;

/**
 * http://www.royin.go.th/?knowledges=%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%AD%E0%B9%88%E0%B8%B2%E0%B8%99%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B9%80%E0%B8%A5%E0%B8%82%E0%B8%97%E0%B8%B5%E0%B9%88%E0%B8%95%E0%B8%B1%E0%B8%A7%E0%B8%97%E0%B9%89%E0%B8%B2e
 * 
 * @author kiatt
 *
 */
public class ThaiBahtUtilTest {

	@Test
	public void testConvertBahtDigitToText() {
		assertEquals("ศูนย์บาท", ThaiBahtUtil.convertToText(new BigDecimal(0)));
		assertEquals("หนึ่งบาท", ThaiBahtUtil.convertToText(new BigDecimal(1)));
		assertEquals("สองบาท", ThaiBahtUtil.convertToText(new BigDecimal(2)));
		assertEquals("สามบาท", ThaiBahtUtil.convertToText(new BigDecimal(3)));
		assertEquals("สี่บาท", ThaiBahtUtil.convertToText(new BigDecimal(4)));
		assertEquals("ห้าบาท", ThaiBahtUtil.convertToText(new BigDecimal(5)));
		assertEquals("หกบาท", ThaiBahtUtil.convertToText(new BigDecimal(6)));
		assertEquals("เจ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(7)));
		assertEquals("แปดบาท", ThaiBahtUtil.convertToText(new BigDecimal(8)));
		assertEquals("เก้าบาท", ThaiBahtUtil.convertToText(new BigDecimal(9)));
	}

	@Test
	public void testConvertBahtTensToText() {
		assertEquals("สิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(10)));
		assertEquals("สิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(11)));
		assertEquals("สิบสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(12)));

		assertEquals("ยี่สิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(20)));
		assertEquals("ยี่สิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(21)));
		assertEquals("ยี่สิบสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(22)));

	}

	@Test
	public void testConvertBahtHundredsToText() {
		assertEquals("หนึ่งร้อยบาท", ThaiBahtUtil.convertToText(new BigDecimal(100)));
		assertEquals("หนึ่งร้อยเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(101)));
		assertEquals("หนึ่งร้อยสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(102)));

		assertEquals("หนึ่งร้อยสิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(110)));
		assertEquals("หนึ่งร้อยสิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(111)));
		assertEquals("หนึ่งร้อยสิบสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(112)));

		assertEquals("หนึ่งร้อยยี่สิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(120)));
		assertEquals("หนึ่งร้อยยี่สิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(121)));
		assertEquals("หนึ่งร้อยยี่สิบสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(122)));

	}

	@Test
	public void testConvertBahtThousandsToText() {
		assertEquals("หนึ่งพันบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000)));
		assertEquals("หนึ่งพันเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(1001)));
		assertEquals("หนึ่งพันสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(1002)));

		assertEquals("หนึ่งพันหนึ่งร้อยสิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(1110)));
		assertEquals("หนึ่งพันหนึ่งร้อยสิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(1111)));
		assertEquals("หนึ่งพันหนึ่งร้อยสิบสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(1112)));

	}

	@Test
	public void testConvertBahtTenThousandsToText() {
		assertEquals("หนึ่งหมื่นบาท", ThaiBahtUtil.convertToText(new BigDecimal(10000)));
		assertEquals("หนึ่งหมื่นเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(10001)));
		assertEquals("หนึ่งหมื่นสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(10002)));
		assertEquals("หนึ่งหมื่นสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(10003)));
	}

	@Test
	public void testConvertBahtHunredThousandsToText() {
		assertEquals("หนึ่งแสนบาท", ThaiBahtUtil.convertToText(new BigDecimal(100000)));
		assertEquals("หนึ่งแสนเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(100001)));
		assertEquals("หนึ่งแสนสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(100002)));
		assertEquals("หนึ่งแสนสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(100003)));
	}

	@Test
	public void testConvertBahtMillionToText() {
		assertEquals("หนึ่งล้านบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000000)));
		assertEquals("หนึ่งล้านเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000001)));
		assertEquals("หนึ่งล้านสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000002)));
		assertEquals("หนึ่งล้านสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000003)));

	}

	@Test
	public void testConvertBahtTenMillionToText() {
		assertEquals("สิบล้านบาท", ThaiBahtUtil.convertToText(new BigDecimal(10000000)));
		assertEquals("สิบล้านเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(10000001)));
		assertEquals("สิบล้านสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(10000002)));
		assertEquals("สิบล้านสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(10000003)));

	}

	@Test
	public void testConvertBahtHundredMillionToText() {
		assertEquals("หนึ่งร้อยล้านบาท", ThaiBahtUtil.convertToText(new BigDecimal(100000000)));
		assertEquals("หนึ่งร้อยล้านเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(100000001)));
		assertEquals("หนึ่งร้อยล้านสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(100000002)));
		assertEquals("หนึ่งร้อยล้านสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(100000003)));

	}

	@Test
	public void testConvertBahtBilliondMillionToText() {
		assertEquals("หนึ่งพันล้านบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000000000)));
		assertEquals("หนึ่งพันล้านเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000000001)));
		assertEquals("หนึ่งพันล้านสองบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000000002)));
		assertEquals("หนึ่งพันล้านสามบาท", ThaiBahtUtil.convertToText(new BigDecimal(1000000003)));
	}

	@Test
	public void randomTest() {
		assertEquals("หนึ่งหมื่นสองพันสามร้อยสี่สิบห้าบาท", ThaiBahtUtil.convertToText(new BigDecimal(12345)));
		assertEquals("ห้าหมื่นสี่พันสามร้อยยี่สิบเอ็ดบาท", ThaiBahtUtil.convertToText(new BigDecimal(54321)));
	}

	@Test
	public void testOverLimitInteger() {
		assertEquals(
				String.format("ไม่สามารถรองรับค่าที่เกินขนาด %s ได้",
						new DecimalFormat("#,###").format(Integer.MAX_VALUE)),
				ThaiBahtUtil.convertToText(new BigDecimal(10000000000L)));

	}

	@Test
	public void testFloatingPoint() {
		assertEquals("สองร้อยสามสิบบาท", ThaiBahtUtil.convertToText(new BigDecimal(230.00)));

		assertEquals("ศูนย์บาทห้าสิบสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(0.50)));

		assertEquals("สิบเอ็ดบาทเจ็ดสิบห้าสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(11.75)));
		assertEquals("สิบสองบาทสี่สิบห้าสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(12.45)));

		assertEquals("ยี่สิบบาทสามสิบเก้าสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(20.39)));
		assertEquals("ยี่สิบเอ็ดบาทสี่สิบเอ็ดสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(21.41)));
		assertEquals("ยี่สิบสองบาทเก้าสิบเก้าสตางค์", ThaiBahtUtil.convertToText(new BigDecimal(22.99)));
	}
}
